<?php
/**
 * Autoload
 *
 * @category     WordPress
 * @package      UF
 * @author       Underscorefunk Design
 * @license      GPL-2.0+
 * @link         https://gitlab.com/Underscorefunk/uf-component-taxonomy
 * @version      1.1.0
 *
 * Description:  A class autoloader for uf-components that loads the highest
 *               version of a matching classes.
 *
 *               The autoloader will look into
 *                  /plugins/{plugin}/classes/...
 *                  /plugins/{plugin}/vendor/uf-...
 *                  /themes/{theme}/classes/...
 *                  /themes/{theme}/vendor/uf-...
 *
 *               The autoloader will skip nested vendor folders
 *                  /plugins/{plugin}/vendor/uf-.../vendor/...
 *                  /themes/{theme}/vendor/uf-.../vendor/...
 *
 *               See below for more details on how to interact with
 *               the uf-autoloader
 *
 * Author:       Underscorefunk Design
 * Author URI:   https://underscorefunk.com
 * Contributors: John Funk (@underscorefunk / underscorefunk.com)
 *
 * Text Domain:  uf
 * Domain Path:  languages
 *
 *
 * Released under the GPL license
 * http://www.opensource.org/licenses/gpl-license.php
 *
 * This is an add-on for WordPress
 * https://wordpress.org/
 *
 * **********************************************************************
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * **********************************************************************
 */

namespace UF;

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// We don't need to run this twice
if ( defined( 'UF_AUTOLOAD_SCAN_COMPLETE' ) && UF_AUTOLOAD_SCAN_COMPLETE ){
	return;
}

/**
 * The purpose of this autoloader is to load the latest version of a UF class across
 * all components and to only load it once. It's common that additional component classes
 * might be requested across multiple plugins. We want the latest version loading when
 * an object of that class is instantiated.
 *
 * This is done by:
 *
 *      1 - index the folders/classes and their versions (this can be cached later, or check
 *          if files were changed since the last update)
 *
 *      2 - find the highest version. This is done by grabbing the @version tag
 *          from the first 1kb of the file. The format is: @version ##.##.##
 *
 *      3 - load the highest version
 *
 * It is expected that your classes will match the following patterns.
 *
 *      // FOR THE MAIN CLASS OF THE COMPONENT
 *      namespace \UF\Component
 *      class foo { ... }
 *
 *      // FOR SUB COMPONENTS
 *      namespace \UF\Component\Foo
 *      class bar { ... }
 *
 * The folder of the package should match the structure of main class namespace + main class classname.
 *
 *      i.e. /vendor/uf-components-foo
 *
 * The package directory "uf-components-foo" will have it's last chunk 'foo' dropped to create a namespace
 * base for the interior class files. This generates the following class names.
 *
 *      /vendor/uf-component-foo/foo.php               ~> class UF\Component\Foo
 *      /vendor/uf-component-foo/foo/bar.php           ~> class UF\Component\Foo\Bar
 *      /vendor/uf-component-foo/foo/fo-shizzle.php    ~> class UF\Component\Foo\Fo_Shizzle
 *
 * Naming conventions:
 *
 *      - Folders names will always use hyphens. They're split on hyphens to create the namespace levels.
 *      - Files should always use hyphens (to be consistent with folder names)
 *      - Hyphens in the composed class name (derived from the package folder name) will have
 *          their hyphens turned into underscores. (fo-shizzle becomes Fo_Shizzle)
 *      - class names will be converted to lowercase for the autoload comparison between files and requested
 *          classes. While the capitalization doesn't matter from a functional stand point, it's good to
 *          be consistent and name with uppercase first letters and underscores to separate words.
 *
 * @todo    Develop a less rigid way of configuring the autoloader so that everything isn't based on 'UF'
 * @todo    Provide support for mu-plugins
 * @todo    Add conditional theme support
 *
 */

// Performance profiling
// $start = microtime(true);

$class_versions = [];

if ( ! defined( 'UF_AUTOLOAD_SCAN_COMPLETE' ) ) {
	
	// Setup the autoload scan config
	$supported_vendor_prefixes = apply_filters(
		"uf_autoload__get_vendor_prefixes",
		['uf-']
	);
	
	$supported_base_dirs = apply_filters(
		"uf_autoload__get_base_dirs",
		[
			WP_CONTENT_DIR . '/themes',
			WP_PLUGIN_DIR,
		]
	);
	
	$supported_class_dirs = apply_filters(
		"uf_autoload__get_class_dirs",
		[
			'classes' => 'root',        // contains core files
			'vendor' => 'container',    // contains additional packages (subpackages)
		]
	);
	
	
	
	// Get a list of the currently active plugins in the plugin-folder/plugin-file.php pattern.
	// Then strip away the file so that we're just using folders. We'll compare those folders to
	// the ones we're traversing and won't traverse folders of plugins that aren't active.
	$active_plugin_folders = (array) get_option( 'active_plugins', [] );
	
	// Add plugins that are being activated
	if ( ! empty( $_REQUEST['action'] ) ) {
		// Activated through the activate link
		if ( $_REQUEST['action'] == 'activate' && ! empty( $_REQUEST['plugin'] ) ) {
			$active_plugin_folders[] = $_REQUEST['plugin'];
		}
		elseif ( $_REQUEST['action'] == 'activate-selected' && ! empty( $_REQUEST['checked'] ) ) {
			$active_plugin_folders = array_merge( $active_plugin_folders,
				$_REQUEST['checked'] );
		}
	}
	
	// Remove filenames from the plugin basenames to just get folders.
	if ( ! empty( $active_plugin_folders ) ) {
		foreach( $active_plugin_folders as &$folder ) {
			$folder = explode( '/', $folder );
			$folder = $folder[0];
		}
	}
	
	$top_level_classes = [];
	
	
	// Loop through the base locations of packages that need to be scanned.
	// This is mainly plugins and themes.
	foreach ( $supported_base_dirs as $base_dir ) {
		
		// Get directories (folders of themes or plugins)
		$dirs = glob( $base_dir . '/*', GLOB_ONLYDIR );
		if ( empty( $dirs ) ) {
			continue;
		}
		
		// Loop through the theme or plugin package directories
		foreach ( $dirs as $dir ) {
			
			// Filter out vendor prefixes that aren't supported
			$package_folder = str_replace( $base_dir . '/', '', $dir );
			$supported      = false;
			foreach( $supported_vendor_prefixes as $prefix ){
				if ( strpos($package_folder, $prefix ) === 0 ){
					$supported = true;
					break;
				}
			}
			if ( ! $supported ) {
				continue;
			}
			
			// Loop through supported class folders to see if php files exist
			// Class type dictates if we should look for matching sub packages
			foreach ( $supported_class_dirs as $class_dir => $class_dir_type ) {
				
				$class_path = $dir . '/' . $class_dir;
				
				// Get and process php files sitting in the root of the class dir
				$package_class_files   = glob( "{$class_path}/*.php" );
				if ( ! empty( $package_class_files ) ) {
					foreach( $package_class_files as $package_class_file ) {
						$key = str_replace( $class_path, $package_folder, $package_class_file );
						if ( ! isset( $top_level_classes[ $key ] ) ) {
							$top_level_classes[ $key ] = [];
						}
						if ( strpos( $key, 'uf-component-' ) === false ) {
							if ( $base_dir == WP_CONTENT_DIR . '/themes' ) {
								$key = str_replace( 'uf-', 'uf-theme-', $key );
							} else if ( $base_dir == WP_PLUGIN_DIR ) {
								$key = str_replace( 'uf-', 'uf-plugin-', $key );
							}
						}
						$top_level_classes[ $key ][] = $package_class_file;
					}
				}

				// Get and process classes from sub folders. This can happen if a folder is a container for
				// other folders, like in the case of /vendor
				$package_class_folders = glob( "{$class_path}/*", GLOB_ONLYDIR );
				if ( ! empty( $package_class_folders ) ) {
					foreach ( $package_class_folders as $package_class_folder ) {
						$package_class_folder_files = glob( "{$package_class_folder}/*.php" );
						if ( ! empty( $package_class_folder_files ) ) {
							foreach( $package_class_folder_files as $package_class_folder_file ) {
								
								if ( $class_dir_type == 'container' ) {
									$key = str_replace( $class_path . '/', '', $package_class_folder_file );
								}
								else {
									$key = str_replace( $class_path, $package_folder, $package_class_folder_file );
									if ( strpos( $key, 'uf-component-' ) === false ) {
										if ( $base_dir == WP_CONTENT_DIR . '/themes' ) {
											$key = str_replace( 'uf-', 'uf-theme-', $key );
										} else if ( $base_dir == WP_PLUGIN_DIR ) {
											$key = str_replace( 'uf-', 'uf-plugin-', $key );
										}
									}
								}
								
								if ( ! isset( $top_level_classes[ $key ] ) ) {
									$top_level_classes[ $key ] = [];
								}
								$top_level_classes[ $key ][] = $package_class_folder_file;
							}
						}
					}
				}
			}
		}
	}
	
	// Get versions for the top level classes
	$versioned_top_level_classes = [];
	if ( ! empty( $top_level_classes ) ) {
		foreach( $top_level_classes as $top_level_class => $files ) {
			
			if ( empty( $files ) ) {
				continue;
			}
			
			foreach( $files as $file ) {
				
				// Class names ($class) will be further sanitized later to actually
				// represent proper namespaced class names (matched on spl_autoload)
				if ( ! isset( $versioned_top_level_classes[ $top_level_class ] ) ) {
					$versioned_top_level_classes[ $top_level_class ] = [];
				}
				
				$version_str = fgets( fopen( $file, 'r' ) );
				preg_match( "/(?:version|v)\s*((?:[0-9]+\.?)+)/i", $version_str, $version_matches );
				$version = '0.0.0';
				if ( isset( $version_matches[1] ) ) {
					$version = $version_matches[1];
				}
				
				$versioned_top_level_classes[ $top_level_class ][ $version ] = $file;
			}

			// Sort by version
			uksort($versioned_top_level_classes[$top_level_class], "version_compare");
			$versioned_top_level_classes[$top_level_class] = array_reverse( $versioned_top_level_classes[$top_level_class] );
		}
	}
	
	$pre_processed_classes = [];
	
	// Great autoloaders for child parts
	if ( ! empty( $versioned_top_level_classes ) ) {
		foreach( $versioned_top_level_classes as $class_file => $paths ) {
			$keys      = array_keys( $paths );
			$path      = $paths[ $keys[0] ];
			$path_info = pathinfo( $path );
			$pre_processed_classes[$class_file] = $path;
			$files = new \RegexIterator(
				new \RecursiveIteratorIterator(
					new \RecursiveDirectoryIterator(
						$path_info['dirname']
					)
				),
				'/^.+\.php$/i',
				\RecursiveRegexIterator::GET_MATCH
			);

			if ( empty( $files ) ) {
				continue;
			}
			
			$class_base = explode( '/', $class_file );
			$class_base = $class_base[0];
			
			foreach ( $files as $file ) {
				// Do nothing if the file found is the top level class file
				if ( empty( $file[0] ) || $file[0] == $path ) {
					continue;
				}
				
				$key        = str_replace( $path_info['dirname'] . '/', '', $file[0] );
				$sub_folder = substr( $key, 0, strpos( $key, '/' ) );
				if ( in_array( $sub_folder, array_keys( $supported_class_dirs ) ) ) {
					continue;
				}
				
				$key                           = $class_base . '/' . $key;
				$pre_processed_classes[ $key ] = $file[0];
			}
		}
		
	}
	
	
	// Let's process the class names versions for easier and more efficient class loading
	$class_versions_parsed = [];
	
	if ( ! empty( $pre_processed_classes ) ) {
		foreach( $pre_processed_classes as $class_name => $class_file ) {
			
			// Sanitize the class name
			
			// first, we explode the class name for some basic parsing.
			// uf-plugin/foo/bar becomes an array of the three parts.
			// We then split the hypens of the first part to figure out what
			// kind of class we're looking at.
			$class_name    = explode( '/', $class_name );
			$class_name[0] = explode( '-', $class_name[0] );
			
			// Validate the correct vendor prefix and class type
			if ( empty( $class_name[0][1] )
			     || $class_name[0][0] !== 'uf'
			     || ! in_array( $class_name[0][1],['plugin', 'component', 'theme' ] )
			) {
				continue;
			}
			
			$class_name[0] = array_slice( $class_name[0], 0, 2 );
			
			// Let's glue the namespace base and type part back together with backslashes
			// which is the correct slash for namespaces.
			$class_name[0] = implode( '\\', $class_name[0] );
			
			// Let's go through each class name part and replace any hyphens with underscores
			// because that's how we roll. No hyphens in namespaces/class-names.
			if ( ! empty( $class_name ) ) {
				foreach( $class_name as &$class_name_part ) {
					$class_name_part = str_replace('-', '_', $class_name_part );
				}
			}
			
			// Let'sglue all of the pieces back together and clear out the php file extension
			$class_name    = implode( '\\', $class_name );
			$class_name    = str_replace( '.php', '', $class_name );
			
			$class_versions_parsed[ $class_name ] = $class_file;
		}
	}
	
	krsort( $class_versions_parsed );
	$class_versions_parsed = array_reverse( $class_versions_parsed );
	
	define( 'UF_AUTOLOAD_SCAN_COMPLETE', true );
	define( 'UF_AUTOLOAD_CLASS_VERSIONS', $class_versions_parsed );
}

// $time_elapsed_secs = microtime(true) - $start;
// echo '<pre>';
// print_r($time_elapsed_secs / 1000 . 'ms');
// echo '</pre>';

spl_autoload_register(
	function( $class_name ) {
		$class_name = strtolower( $class_name );
		
		if ( ! empty( UF_AUTOLOAD_CLASS_VERSIONS[ $class_name ] ) ) {
			
			if ( file_exists( UF_AUTOLOAD_CLASS_VERSIONS[ $class_name ] ) ) {
				require_once UF_AUTOLOAD_CLASS_VERSIONS[ $class_name ];
				return;
			}
			
		}
		
	}
);

